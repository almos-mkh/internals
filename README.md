## Running ALMOS-MKH on the TSAR LETI machine

*Written in August 2018 by Nicolas Phan.*

## Context

TSAR is a many-core architecture developed by the SOC-Lip6 department in Sorbonne Sciences Université, and ALMOS-MKH is an operating system created in the same department for the study of scaling operating systems on such manycore architectures.
For the moment, there is a virtual prototype of TSAR IOB and TSAR LETI (both are similar architecture
with yet a few differences) and an actual prototype of the TSAR LETI architecture.
This document aims at explaining how to run ALMOS-MKH on all three prototypes.

## General principles

The virtual prototypes of the TSAR architecture are hardware descriptions in SystemC, using a library of
SystemC hardware components called Soclib. There are currently two variants of TSAR : IOB and LETI.

There are four entities involved in the execution of an OS on TSAR :
 - The  **preloader**, the first program to be executed by TSAR at boot and in charge of
loading the operating system (ALMOS-MKH, NetBSD, Linux...).
 - The **bootloader**, which is different for each OS and is charge of loading the kernel
 - The **kernel**, the core of the OS
 - The **applications** running on the OS
 
When installing the ALMOS/TSAR environment on the next step, you will install all four entities, amongst other things.

## Setup

First of all, you have to install the environment for developing ALMOS-MKH on TSAR LETI.
To do so, use the installation script [setup.sh](https://framagit.org/projet-l3/code/blob/master/setup.sh).
This script will install the different components :
 - `./almos-mkh` - The ALMOS-MKH operating system
   - `./almos-mkh/boot` - The bootloader for ALMOS-MKH
   - `./almos-mkh/kernel` - The kernel of ALMOS-MKH
   - `./almos-mkh/user` - Various user applications to run on ALMOS-MKH.
 - `./tsar` - The different virtual prototypes of the TSAR architecture
   - `./tsar/platforms` - The vitual prototypes of the differents versions of TSAR (including IOB and LETI)
   - `./tsar/softs/tsar_boot` - The preloader, first program to be executed and in charge  ALMOS
 - `./systemc-2.3.1` - SystemC, the hardware simulation environment.
 - `./soclib` - Soclib, a library of hardware components description in SystemC
 - `./compilo` - The cross-compiler for Mips32 architecture

Then edit the `env.sh` and adjust the different environment variables, you can add them to your `~/.bashrc`
to apply the changes permanently.

### Setup troubleshoot

**TODO**

## Running ALMOS on TSAR

First, we have to determine different parameters of the target architecture, these parameters have to be in the file `almos-mkh/params-hard.mk`.

In `almos-mkh/`, you have two presets of parameters `params-hard-simu.mk` and `params-hard-real.mk` that you can begin with, you may want to symlink them :

	cd almos-mkh/
	ln -fs params-hard-simu.mk params-hard.mk

This file contains the list of parameters used to determine the platform ALMOS-MKH will run on.
-  `PLATFORM` - This parameter must contain the path of the prototype (`./tsar/platforms/tsar_generic_XXX`)
   you want to run ALMOS on. (whether you want to run on a virtual or real prototype doesn't matter here)
 - `X_SIZE` - The number of columns of clusters
 - `Y_SIZE` - The number of rows of clusters
 - `NPROCS` - The number of cores per cluster
 - `NB_TTYS` - The number of terminal you want (at least 2, one for the kernel debug term, one with a shell)
 - `IOC_TYPE` - The type of disk you want to use
 - `SYS_CLK` - The frequency of the system (25MHz in simulation, 600MHz for the real machine)

Now you have configured the target architecture for ALMOS

	cd almos-mkh/
	make

The build of almos-mkh will, among other things, generate `hard_config.h` from `tsar/platforms/tsar_generic_xxx/arch_info.py`, the former of which is used in the generation of the virtual prototype and the preloader.
Consequently, you must compile **in this order** almos-mkh, then the virtual prototype (tsar leti or iob), then the preloader whenever you change `arch_info.py`.

The next steps depend ocp -r vci_spi vci_spi.bakrn whether you want to run ALMOS on a virtual or actual prototype.

### Running ALMOS on a simulation (virtual prototype)

Build the virtual hardware platform and the preloader, and execute the simulation :

	cd ./tsar/platforms/tsar_generic_xxx
	make
	cd ./tsar/softs/tsar_boot/)
	make
	cd ./tsar/platforms/tsar_generic_xxx
	./simul.x

Then you should see N xterms opening and the preloader, bootloader then kernel printing messages.
The preloader messages begin with `[RESET]`, the bootloader with `[BOOT]` and the kernel with `[DBG]`.

### Running ALMOS on the actual prototype

To boot almos on the real TSAR machine, repeat the same steps as above (except the last part, only done for a simulation).

Then, you will need two SD cards, one for storing the TSAR preloader and the other for the OS (including bootloader)

**NOT ALL SD CARDS WORK ON TSAR, USE THE KINGSTON 4GB SD CARD FOR PRELOADER AND THE MAXL 16GB SD CARD FOR ALMOS**

	Put the first SD card and format it into FAT.
	Copy paste the preloader from ./tsar/softs/tsar_boot/preloader.elf
	to the SD card, and name it tsarboot.elf /!\ The name is important /!\

Then put the second SD card and :

	cd ./almos-mkh/hdd
	sudo ./cut.sh /dev/sdX (with sdX the device corresponding to the SD card)

The `cut.sh` script will burn the disk image of ALMOS (almos-mkh-hdd.dmg) into the SD card.

On TSAR, you have 3 output serial ports (from top to bottom on the machine) :
- Microblaze output
- 16550 output
- Cluster 00's SPI output

and 3 SD card slots (from top to bottom on the machine) :
- Preloader SD card
- Broken SD card slot
- OS SD card

We'll begin with the preloader,

	Plug the preloader and OS SD card into the corresponding slots
	Now plug the Cluster 00's SPI output (and also the Microblaze output,
	it can be useful for debug) into your computer's USB ports

*I will assume that the corresponding devices are `/dev/usb0` and `/dev/usb1`*

#### If you want to observe the raw outputs with minicom :
Open a new terminal and execute :

	minicom /dev/usb0

On minicom, configure your setting like so :

	Type 'Ctrl-A Z O'
	Choose 'Serial port setup'

Then enter these settings :

	Serial Device : /dev/usb0
	Bps/Par/Bits  : 115200 8N1
	Hardware Flow Control : No
	Software Flow Control : No

You can then select
			
	Save setup as _dev_usb0

And finally, enable carriage returns with

	Ctrl-A Z U

Now for unknown reasons, you have to unplug and replug the USB cable to see input on minicom...

	Unplug and replug the USB cable connecting TSAR and your computer

Repeat these steps with /dev/usb1 on yet another terminal, push the On/Off button of the TSAR machine
and you should see ALMOS booting.
			
#### If you want to use the several virtual terminals :

**TODO**

### Special files involved in the compilation/execution process

 - `almos-mkh/params-hard.mk` - This configuration file enables you to choose the main characteristics of the target architecture of the virtual prototype.
 - `almos-mkh/params-soft.mk` - Same as params-hard.mk, but for the software configuration parameters
 - `almos-mkh/hdd/virt_hdd.dmg` - The disk image of ALMOS-MKH (kernel + bootloader + user apps), used by both the virtual and actual prototypes of TSAR.
 - `almos-mkh/hard_config.h` - This file is generated by a `make` on almos, and its content depends on the content of of the `arch_info.py` file of the target architecture. This file is used by both the OS and the virtual prototype.
 - `tsar/platforms/tsar_generic_xxx/arch_info.py` - This file (one per architecture) contains the specificities of the target architecture and is used almos to generate the `hard_config.h` file.

## Debugging ALMOS-MKH

The TSAR virtual prototypes provide an interface called GDBServer that can be used to debug the
execution of almos-mkh. GDBServer is integrated to the virtual prototype and expects a gdb client to
connect to it (it is similar to kgdb), the debugging then happens on a gdb session through the call to
various services offered by GDBServer.

To get started :

For further information, see the [GDB Server Documentation](http://www.soclib.fr/trac/dev/wiki/Tools/GdbServer).
