
# ----------------------------------------------------
# --------------- Portage-related setup---------------
# ----------------------------------------------------
# Setup relative to the porting on LETI
if [ ! -d portage ]; then
  git clone https://framagit.org/NicolasPhan/portage.git portage
fi
pushd tsar/softs/
mv tsar_boot tsar_boot.bak
ln -nfs ../../portage/tsar_boot tsar_boot
popd

pushd tsar/platforms/
mv tsar_generic_leti tsar_generic_leti.bak
ln -nfs ../../portage/tsar_generic_leti tsar_generic_leti
popd

# A simple symlink doesn't work (the virtual prototype doesn't compile) so we copy the folder
pushd tsar/modules/
rm -rf vci_spi # don't keep a vci_spi_bak folder, it will get compiled with tsar and compiling will fail
cp -r ../../portage/vci_spi ./
popd

# Switch to portage3
pushd almos-mkh
git checkout portage3
if [ -f params-hard.mk ] || [ -L params-hard.mk ]; then
  rm params-hard.mk
  ln -nfs params-hard-simu.mk params-hard.mk
fi
popd

sed -i "s#^ARCH.*#ARCH      = `pwd`/tsar/platforms/tsar_generic_iob#" almos-mkh/params-hard-simu.mk
sed -i "s#^ARCH.*#ARCH      = `pwd`/tsar/platforms/tsar_generic_iob#" almos-mkh/params-hard-real.mk
sed -i "s#^ALMOSMKH_DIR.*#ALMOSMKH_DIR = `pwd`/almos-mkh#" almos-mkh/params-soft.mk

# Add symbolic links
ln -nfs $(pwd)/almos-mkh/hdd/virt_hdd.dmg tsar/platforms/tsar_generic_iob/almos_virt_hdd.dmg
ln -nfs $(pwd)/almos-mkh/hdd/virt_hdd.dmg tsar/platforms/tsar_generic_leti/virt_hdd.dmg
ln -nfs $(pwd)/almos-mkh/hard_config.h tsar/platforms/tsar_generic_iob/hard_config.h
ln -nfs $(pwd)/almos-mkh/hard_config.h tsar/platforms/tsar_generic_leti/hard_config.h

# add a segs_set argument in virtual tsar iob

# double ram size 8Mo -> 16Mo on IOB and then make clean EVERYTHING, and ONLY THEN make everything

# creat a hdd dir in almos-mkhglobal.c
