# Writing a syscall for Almos-mkh

TODO: blabla about Makefile architecture making some fancy ascii Filesystem view

Reading level: licence 3 or enthusiastic hacker.

Let's write a `hello_kern()` syscall.

## What is a syscall?

A syscall is a *service* offered by the kernel to userland accessible throught a switch in kernel code and space from userland.
Exemple: `write`, `read` are syscalls.

To **call** the kernel we have to use a special instruction exemple on intel *x86_64* `syscall` or on *mips32* `syscall` or on *armv7* `swi` to do a **software interruption**
also called a **trap**.

This abstraction is provided by `hal_do_syscall()` is already defined in `kernel/hal_user.h`, if not provide one in `hal/_your_arch_/hal_do_syscall.c` and adapt `hal/Makefile`.
`hal_do_syscall()` abstract the instruction trap and the argument register affectation policy of the `abi`.

If the look at the *prototype* of this function:

```c
int hal_user_syscall( reg_t service_num,
                      reg_t arg0,
                      reg_t arg1,
                      reg_t arg2,
                      reg_t arg3 );
```

We need to supply a `service_num`, this must be a 32 bit integer on mips32 as exemple. So let's add one in `kernel/syscall/shared_include/syscalls_numbers.h`

First we have to add a syscall *number*, in the *enum* `kernel/syscalls/shared_include/syscalls_numbers.h`
```c
enum {
  SYS_THREAD_EXIT 1,
  // [...]
  SYS_UNDEFINED 49,
  SYS_NR 50,
};
```

We will pick the 49 number for `hello_kern`,

```diff
enum {
  SYS_THREAD_EXIT 1,
  // [...]
-  SYS_UNDEFINED 49,
+  SYS_HELLO 49, 
  SYS_NR 50,
};
```

We must keep concistant with this `enum` the *jump table* in `/kernel/kern/do_syscall.c`

```diff
static const sys_func_t syscall_tbl[SYSCALLS_NR] = 
{
  sys_thread_exit,        // 0
  // [...]
-  sys_undefined,         // 49
+  sys_hello,             // 49
```

Adding a pretty printer in `char * syscall_str( uint32_t index )` also is a good idea for debugging purpose.

Now let's write the syscall itself let's write `kernel/syscalls/sys_hello.c`

```c
void sys_hello ( void )
{
  printk("Hello from kernel space\n");
  return 0;
}
```

Note: if your syscall need to export macro or define place them in `kernel/syscalls/shared_include/shared_yourSyscall.h`.

Now you can wrap this syscall in a library in userland:

```
#include <syscall.h>
void helloKern(void) {
  hal_do_syscall(SYS_HELLO, 0,0,0,0);
}
```

Becareful to includes the kernel header with the *syscall numbers* enum.

Next step: Depending of what you need to do you will have to use internals of the kernel like *hal* functions or *libk* abstraction or use
devices.

Also think twice: *can I need a syscall?* In Unix way we try to reuse heavily abstraction like filesystems `/dev/...`, `/proc/...` before adding a syscall for doing one thing.
