# Changement de style de programation pour Almos-MKH.

Ce document porte sur une proposition de changement de style de programmation pour la
déclaration des variables dans le projet ALMOS-MKH.

## Motivation

Actuellement le projet utile un «style» de programmation proche de ceux utilisé en C standard 89 (*C89*),
avec des déclarations des variables locales avant initialisation dans le block le plus haut de la fonction.

### Exemple concret

```C
{
  int32_t a;
  int32_t b;
  int32_t c;
  a = 3;
  b = 2;
  {  
    c = a + b;
  }
}
```

Un changement concret pourrait être d'adopter un style favorisant les déclarations + initialisations et de
garder les déclarations sans initialisations dans les cas ou c'est réelement plus clair.

Le second changement est de rapproché au plus près de son usage la déclaration d'une variable.

Cela permet de rendre local a des blocks certaines variables et evite des déclarations "globales" a une fonction.

```C
{
  int32_t a = 3;
  int32_t b = 2;
  {
    int32_t c = a + b;
  }
}
```

## Motivations principales:

Eviter les déclarations sans intialisation eviter les usages (rares) de variables non itialisé.

Rechercher la localité au maximum pour les variables est un avantage pour la lecture du code et peut permettre d'éviter des bugs liée à des erreurs d'utilisation ou de portée de la variable.

Autre avantage cela rends le code plus fluide à lire se tendant à l'essentiel. Permet d'avoir les commentaires de déclaration au plus près de l'usage de la variable.

## Désaventages

Calculer l'espace en terme de bytes des variables locales d'une fonction est plus complexe. Cependant ALMOS-MKH a vocation à être compiler en mode optimisation activé cela relativise ce point drastiquement.

## Aspects connexes

Actuellement nous sommes en norme *Gnu89* cette norme nous permet cette souplesse. La norme *C89* non. Si on souhaite appliquer cette RFC et avoir comme norme C «standard» il faut trancher maintenant pour *C99* ou plus.

## Aller plus loin

Une piste pourrait être de noter `const` certaines variables locale la ou cela fait sens. Afin de gagner une *relative* sureté.

Utiliser `const` comme un contrat dans le prototype des fonctions *terminales* qui ne modifient pas le pointeur donné en argument, ou bien sur des variables locales non modifiées. Ou sur l'implementation de fonctions terminales pour eviter des erreurs de programation.

Notament la ou l'ont utilise des `#define` une variable en `const` serait plus pertinante car on obtiens un *symbole* débuggable dans GDB.

## Cas concret


```C
// process.c
// Version actuelle 
void process_txt_attach( process_t * process,
                         uint32_t    txt_id )
{
    xptr_t      chdev_xp;     // extended pointer on TXT_RX chdev
    cxy_t       chdev_cxy;    // TXT_RX chdev cluster
    chdev_t *   chdev_ptr;    // local pointer on TXT_RX chdev
    xptr_t      root_xp;      // extended pointer on list root in chdev
    xptr_t      lock_xp;      // extended pointer on list lock in chdev

#if DEBUG_PROCESS_TXT
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_PROCESS_TXT < cycle )
printk("\n[DBG] %s : thread %x enter for process %x / txt_id = %d  / cycle %d\n",
__FUNCTION__, CURRENT_THREAD, process->pid, txt_id, cycle );
#endif

    // check process is in owner cluster
    assert( (CXY_FROM_PID( process->pid ) == local_cxy) , __FUNCTION__ ,
    "process descriptor not in owner cluster" );

    // check terminal index
    assert( (txt_id < LOCAL_CLUSTER->nb_txt_channels) ,
    __FUNCTION__ , "illegal TXT terminal index" );

    // get pointers on TXT_RX[txt_id] chdev
    chdev_xp  = chdev_dir.txt_rx[txt_id];
    chdev_cxy = GET_CXY( chdev_xp );
    chdev_ptr = GET_PTR( chdev_xp );

    // get extended pointer on root & lock of attached process list
    root_xp = XPTR( chdev_cxy , &chdev_ptr->ext.txt.root );
    lock_xp = XPTR( chdev_cxy , &chdev_ptr->ext.txt.lock );

    // insert process in attached process list
    remote_spinlock_lock( lock_xp );
    xlist_add_last( root_xp , XPTR( local_cxy , &process->txt_list ) );
    remote_spinlock_unlock( lock_xp );

#if DEBUG_PROCESS_TXT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_PROCESS_TXT < cycle )
printk("\n[DBG] %s : thread %x exit for process %x / txt_id = %d / cycle %d\n",
__FUNCTION__, CURRENT_THREAD, process->pid, txt_id , cycle );
#endif

} // end process_txt_attach()

// Deviendrait:
void process_txt_attach( process_t * process,
                         uint32_t    txt_id )
{    
#if DEBUG_PROCESS_TXT
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_PROCESS_TXT < cycle )
printk("\n[DBG] %s : thread %x enter for process %x / txt_id = %d  / cycle %d\n",
__FUNCTION__, CURRENT_THREAD, process->pid, txt_id, cycle );
#endif

    // check process is in owner cluster
    assert( (CXY_FROM_PID( process->pid ) == local_cxy) , __FUNCTION__ ,
    "process descriptor not in owner cluster" );

    // check terminal index
    assert( (txt_id < LOCAL_CLUSTER->nb_txt_channels) ,
    __FUNCTION__ , "illegal TXT terminal index" );

    // get pointers on TXT_RX[txt_id] chdev
    // extended pointer on TXT_RX chdev
    xptr_t      chdev_xp = chdev_dir.txt_rx[txt_id];
    chdev_cxy = GET_CXY( chdev_xp );
    chdev_ptr = GET_PTR( chdev_xp );
    
    // get extended pointer in chdev on root  & lock of attached process list
    xptr_t root_xp = XPTR( chdev_cxy , &chdev_ptr->ext.txt.root );

    // insert process in attached process list
    // extended pointer on list lock in chdev
    xptr_t lock_xp = XPTR( chdev_cxy , &chdev_ptr->ext.txt.lock );
    remote_spinlock_lock( lock_xp );
    xlist_add_last( root_xp , XPTR( local_cxy , &process->txt_list ) );
    remote_spinlock_unlock( lock_xp );

#if DEBUG_PROCESS_TXT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_PROCESS_TXT < cycle )
printk("\n[DBG] %s : thread %x exit for process %x / txt_id = %d / cycle %d\n",
__FUNCTION__, CURRENT_THREAD, process->pid, txt_id , cycle );
#endif

} // end process_txt_attach()
```
