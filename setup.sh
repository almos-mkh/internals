#!/bin/bash
set -u -e -x

#manual_edit=1
get_native_compiler=1

#This script setup all environnement for almos-mkh
#Requirement : curl svn wget make sed tar libsdl-1.2
#Touche a ~/.soclib/global.conf, ~/.subversion/servers
binutils_version=2.29
gcc_version=8.3.0
gdb_version=8.2.1
systemc_version=2.3.2
systemc="systemc-${systemc_version}"

arch="mipsel"
triplet=${arch}-unknown-elf
deps_dir="deps"

# ----------------------------------------------------
# --------------- Setup systemc ----------------------
# ----------------------------------------------------
if [ ! -d $systemc ]; then
  echo "Downloading SystemC"
  if [ ! -f "${systemc}.tar.gz" ]; then
    curl -O "https://www.accellera.org/images/downloads/standards/systemc/${systemc}.tar.gz"
  fi
  tar xf "${systemc}.tar.gz"

  pushd $systemc
  mkdir -p obj
  pushd obj
  echo "Installing SystemC."
  ../configure
  make -j4
  make install
  echo "installation ${systemc} OK"
  popd
  popd
fi # End setup SystemC

# ----------------------------------------------------
# --------------- Setup soclib ----------------------
# ----------------------------------------------------
if [ ! -d "soclib" ]; then
  echo "Installing SocLib"

  # TODO: Remove me in the future if useless
  # remove compression in ~/.subversion/servers
  # by uncommenting line "http-compression = no"
  # if [ manual_edit -eq 0 ]; then
  #   sed -i "s/# http-compression = no/http-compression = no/" ~/.subversion/servers
  # fi

  svn co --non-interactive --trust-server-cert https://www.soclib.fr/svn/trunk/soclib soclib
  # modifier le path pour ajouter soclib-cc
  # ou modifier le Makefile pour obtenir soclib-cc

  pushd "soclib/utils/src"
  make -j4
  make install
  popd
fi

#TODO ecrire dans ~/.soclib/soft_compilers.conf
#Setup crosscompile compilo
binaire_path=$(pwd)/bin
export PATH=${binaire_path}:$PATH
mkdir -p ${deps_dir}
pushd ${deps_dir}
mkdir -p build/$triplet/{binutils,gcc,gdb,gdb} src bin


# ----------------------------------------------------
# --------------- Setup gcc, gdb, binutils -----------
# ----------------------------------------------------
# Telechargement des sources de gcc, gdb et binutils.
echo "Installing gdb and binutils..."
pushd src

if [ ! -f gcc-${gcc_version}.tar.xz ]; then
  curl -O "https://ftp.gnu.org/gnu/gcc/gcc-${gcc_version}/gcc-${gcc_version}.tar.xz"
  tar xf gcc-${gcc_version}.tar.xz
fi

if [ ! -f gdb-${gdb_version}.tar.xz ]; then
    curl -O "https://ftp.gnu.org/gnu/gdb/gdb-${gdb_version}.tar.xz"
    tar xf gdb-${gdb_version}.tar.xz
fi

if [ ! -f binutils-${binutils_version}.tar.xz ]; then
    curl -O "https://ftp.gnu.org/gnu/binutils/binutils-${binutils_version}.tar.xz"
    tar xf binutils-${binutils_version}.tar.xz
fi

popd # src

if [ ! -f ${binaire_path}/${triplet}-gcc ]; then
    if [ ! -d gcc-${gcc_version} ]; then
        echo "pouet"
    fi
    echo "download gcc dependancies"
    # Install necessary packaged for building gcc
    pushd src/gcc-${gcc_version}
    ./contrib/download_prerequisites
    popd

  pushd build/${triplet}/gcc
  ../../../src/gcc-${gcc_version}/configure --prefix=${binaire_path} \
    --program-suffix="-${gcc_version}" \
    --target=${triplet} --program-prefix=${triplet}- \
    --disable-nls --disable-werror --enable-languages=c --disable-libssp \
    --disable-threads --without-headers
  make -j4
  make install
  popd # gcc
else
    echo "gcc is already installed"
fi

if [ ${get_native_compiler} -eq 1 ]; then
  pushd build/native/gcc
  ../../../src/gcc-${gcc_version}/configure --prefix=${binaire_path} \
    --program-suffix="-${gcc_version}" \
    --disable-nls --disable-werror --enable-languages=c --disable-libssp \
    --disable-threads --without-headers
  make -j4
  make install
  popd
fi

if [ ! -f ${binaire_path}/${triplet}-gdb ]; then
  pushd build/${triplet}/gdb
  if [ $get_native_compiler -eq 1 ]; then
    ../../../src/gdb-${gdb_version}/configure \
      CXX=../../../bin/bin/g++-${gdb_version} --prefix=${binaire_path} \
      --target=${triplet} --program-prefix=${triplet}-
  else
    ../../../src/gdb-${gdb_version}/configure \
      --prefix=${binaire_path} \
      --target=${triplet} --program-prefix=${triplet}-
  fi
  make -j4
  make install
  popd
else
    echo "gdb is already installed"
fi

if [ ! -f ${binaire_path}/${triplet}-ar ]; then
  pushd build/${triplet}/binutils
  curl -O "https://ftp.gnu.org/gnu/binutils/binutils-${binutils_version}.tar.xz"
  tar xf binutils-${binutils_version}.tar.xz

  pushd build/${triplet}/binutils
  ../../../src/binutils-${binutils_version}/configure \
    --prefix=${binaire_path} --target=$triplet \
    --program-prefix=$triplet- \
    --disable-nls --disable-werror
  make -j4
  make install
  popd
else
    echo "Binutils is already installed"
fi
popd # deps

# ----------------------------------------------------
# --------------- Setup almos-mkh --------------------
# ----------------------------------------------------
#setup almos-mkh
# svn?
# Changing access rights so that students can pull from the framagit ?

if [ ! -d almos-mkh ]; then
  echo "installing Almos-mkh"
  git clone https://framagit.org/projet-l3/mirror-amk.git almos-mkh
  pushd almos-mkh
  git checkout -b modif
  popd
else
    echo "Almos-mkh is already installed"
fi

almos_path=$(pwd)/almos-mkh
export HARD_CONFIG_PATH=$almos_path

# ----------------------------------------------------
# --------------- Setup TSAR -------------------------
# ----------------------------------------------------
svn co --non-interactive --trust-server-cert https://www-soc.lip6.fr/svn/tsar/trunk tsar
mkdir -p ~/.soclib/
echo "config.addDescPath(\"$(pwd)/tsar\")" >> ~/.soclib/global.conf

pushd tsar/platforms/tsar_generic_iob
ln -nfs ${almos_path}/hard_config.h hard_config.h
ln -nfs ${almos_path}/hdd/virt_hdd.dmg almos_virt_hdd.dmg
popd

# add mtools_skip_check=1 to .mtoolsrc

echo "Installation finie! Happy hacking ♥"

#build env
if [ ! -f ./env.sh ]; then
  # Export SystemC includes path and dynamic library
  echo "export SYSTEMC=$(pwd)/${systemc}/" > env.sh
  echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:$(pwd)/${systemc}/lib-linux64/" >> env.sh
  # Update path with soclib tools
  echo "export PATH=${binaire_path}:$(pwd)/soclib/utils/bin:\$PATH" >> env.sh
  echo "export HARD_CONFIG_PATH=$almos_path" >> env.sh
  # aliases for ease of hacking
  echo "alias simu=\"cd $(pwd)/tsar/platforms/tsar_generic_iob\"" >> env.sh
  echo "alias amk=\"cd $(pwd)/almos-mkh\"" >> env.sh
  #TODO update almos-mkh, tsar, soclib

  echo "Usage: source env.sh"
fi
