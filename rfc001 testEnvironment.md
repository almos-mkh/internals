# Mise en place d'un environnement de test pour ALMOS-MKH

Ce document porte sur la proposition d'une mise en place d'un environnement
et d'une méthodologie de test pour ALMOS-MKH.

## Motivation

A l'état actuel, ALMOS-MKH est à un état de fiabilisation et un des principaux
obstacles à l'évolution de l'OS est la difficulté à corriger les bugs qui
se manifestent, la difficulté est avant tout de *cerner l'origine* des ces bugs.

L'idéal serait de pouvoir tester individuellement les différentes couches
logicielles d'ALMOS-MKH via des tests unitaires afin de pouvoir corriger les
bugs liés à chacune d'entre elles, le fait que les tests soient individuels
permet de mieux cibler l'origine d'un bug qui apparaitrait lors d'un test.

## Approche

La méthodologie de test se ferait progressivement, car il n'est pas possible
de tout tester en même temps. Nous pouvons effectuer les tests de bas en haut,
en commençant par la hal et les structures de données (XLIST), Macro,
   qui possèdent beaucoup moins de dépendances que d'autres
parties du système, et en continuant vers des couches logicielles plus hautes.

Le but étant de pouvoir supposer que, lorsqu'un test échoue, le problème vienne
de l'unité testée et non pas d'une dépendance déjà testée auparavant.

Une autre possibilité est de tester en userland via les appels system via un jeu
de test d'appels systèmes existant... Cependant vu que nous ne somment pas POSIX
l'interet est plus limité.

## Concretement

Écrire des test pour:
- hal
- libk (fonctions "pures") et allocation
- XPTR
- XLIST

Écrire/récuperer des test pour nos appels systèmes.

## Exemples

Bugs récents...
